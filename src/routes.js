/* eslint-disable import/no-cycle */
import Auth from './pages/Auth';
import Main from './pages/Main';
import Registration from './pages/Registration';
import Welcome from './pages/Welcome';
import {
  MAIN_ROUTE,
  AUTH_ROUTE,
  REGISTRATION_ROUTE,
  WELCOME_ROUTE,
} from './utils/const';

export const authRoutes = [
  {
    path: MAIN_ROUTE,
    Component: Main,
  },
];

export const publicRoutes = [
  {
    path: AUTH_ROUTE,
    Component: Auth,
  },
  {
    path: REGISTRATION_ROUTE,
    Component: Registration,
  },
  {
    path: WELCOME_ROUTE,
    Component: Welcome,
  },
];
