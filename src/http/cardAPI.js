import { $authHost } from './index';

// eslint-disable-next-line import/prefer-default-export
export const fetchAllCardsWithTags = async () => {
  const { data } = await $authHost.get('api/card/withCards');
  return data;
};

export const createNewCard = async (url, collectionId) => {
  const { data } = await $authHost.post('api/card/', { url, collectionId });
  return data;
};
export const deleteCurrentCard = async (cardId) => {
  const { data } = await $authHost.delete(`api/card/${cardId}`);
  return data;
};
export const editCard = async (newCard) => {
  const {
    id, header, content, url,
  } = newCard;
  const { data } = await $authHost.put('api/card/', {
    id, header, content, url,
  });
  return data;
};
export const fetchAllUserCards = async () => {
  const { data } = await $authHost.get('api/card/');
  return data;
};
