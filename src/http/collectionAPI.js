import { $authHost } from './index';

// eslint-disable-next-line import/prefer-default-export
export const fetchCollectionsWithCards = async () => {
  const { data } = await $authHost.get('api/collection/withCards');
  return data;
};

export const createNewCollection = async (title) => {
  const { data } = await $authHost.post('api/collection/', { title });
  return data;
};
export const editCollection = async (newCollection) => {
  const {
    id, title,
  } = newCollection;
  const { data } = await $authHost.put('api/collection/', {
    id, title,
  });
  return data;
};
export const fetchOnlyUserCollections = async () => {
  const { data } = await $authHost.get('api/collection/');
  return data;
};

export const deleteCollection = async (collectionId) => {
  const { data } = await $authHost.delete(`api/collection/${collectionId}`);
  return data;
};
