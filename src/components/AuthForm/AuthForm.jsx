/* eslint-disable import/no-cycle */
/* eslint-disable no-shadow */
import React, { useContext, useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { observer } from 'mobx-react-lite';
import { useHistory, NavLink } from 'react-router-dom';
import {
  Formik,
  Form,
} from 'formik';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';

import { authSchema } from '../../utils/validations/validations';
import styles from './AuthForm.module.css';
import { login } from '../../http/userAPI';
import { MyContext } from '../../index';
import { MAIN_ROUTE } from '../../utils/const';
import { authInitialValues } from '../../utils/validations/const';

const AuthForm = observer(() => {
  const { user } = useContext(MyContext);
  const history = useHistory();
  const [open, setOpen] = useState(false);
  const [alertMessage, setAlertMessage] = useState('');

  async function loginUser(loginValue, passwordValue) {
    try {
      const data = await login(loginValue, passwordValue);
      if (data instanceof Error) {
        return data;
      }
      user.setUser({
        email: data.email,
        id: data.id,
        role: data.role,
        iat: data.iat,
        exp: data.exp,
      });
      user.setIsAuth(true);
      history.push(MAIN_ROUTE);
      return data;
    } catch (error) {
      return error.message;
    }
  }
  return (
    <div className={styles.outerWrapper}>
      <div className={styles.wrapper}>
        <div className={styles.header}>
          <Button onClick={() => history.push('/welcome')} style={{ marginRight: '20px' }} variant="contained" color="primary">На главную</Button>
        </div>
        <div className={styles.container}>
          <div className={styles.formHeader}>
            Авторизация
          </div>
          <Formik
            className={styles.formContainer}
            initialValues={authInitialValues}
            validationSchema={authSchema}
            onSubmit={async (values) => {
              try {
                const response = await loginUser(values.login, values.password);
                if (response instanceof Error) {
                  setOpen(true);
                  setAlertMessage(response.message);
                } else {
                  history.push('/main');
                }
              } catch (error) {
                console.log(error.message);
              }
            }}
          >
            {({
              values,
              handleChange,
              errors,
              touched,
              handleBlur,
            }) => (
              <Form className={styles.form}>
                <div className={`${styles.inputContainer} ${styles.mb10}`}>
                  <TextField
                    fullWidth
                    id="login"
                    name="login"
                    label="Логин"
                    value={values.login}
                    onChange={handleChange}
                    error={touched.login && Boolean(errors.login)}
                    helperText={touched.login && errors.login}
                    onBlur={handleBlur}
                  />
                </div>
                <div className={`${styles.inputContainer} ${styles.mb10}`}>
                  <TextField
                    fullWidth
                    id="password"
                    label="Пароль"
                    name="password"
                    type="password"
                    value={values.password}
                    onChange={handleChange}
                    error={touched.password && Boolean(errors.password)}
                    helperText={touched.password && errors.password}
                    onBlur={handleBlur}
                  />
                </div>
                <div className={styles.formFooter}>
                  <Button type="submit" variant="contained" color="primary">
                    Войти
                  </Button>
                </div>
                <div>
                  Нет аккаунта?
                  {' '}
                  <NavLink to="/registration" style={{ cursor: 'pointer', color: 'blue' }}>Зарегистрируйтесь!</NavLink>
                </div>
              </Form>
            )}
          </Formik>
          <Snackbar open={open} autoHideDuration={2000} onClose={() => setOpen(false)}>
            <Alert onClose={() => setOpen(false)} severity="error">
              {alertMessage}
            </Alert>
          </Snackbar>
        </div>
      </div>
    </div>
  );
});
export default AuthForm;
