/* eslint-disable no-shadow */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from 'react';
import styles from './TabItem.module.css';

export default function TabItem(props) {
  const {
    label,
    id,
    active,
    setActiveTabIndex,
  } = props;

  function onClickHandler(event) {
    setActiveTabIndex(Number(event.target.id));
  }

  function getStyles(active) {
    if (active) {
      return `${styles.tabItem} ${styles.activeTab}`;
    }

    return `${styles.tabItem}`;
  }
  return (
    <div
      onClick={onClickHandler}
      id={id}
      className={getStyles(active)}
    >
      {label}
    </div>
  );
}
