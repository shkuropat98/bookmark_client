/* eslint-disable import/no-cycle */
/* eslint-disable jsx-a11y/control-has-associated-label */
/* eslint-disable max-len */
import React, { useContext, useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import styles from './Modal.module.css';
import { MyContext } from '../../index';
import { editCard } from '../../http/cardAPI';

export default function Modal({ activeCard }) {
  const { tabs } = useContext(MyContext);

  const [header, setHeader] = useState(activeCard.header);
  const [description, setDescription] = useState(activeCard.content);
  const [link, setLink] = useState(activeCard.url);

  const closeModal = () => {
    tabs.setOpenEditModal(false);
  };
  const handleHeaderChange = (event) => {
    setHeader(event.target.value);
  };
  const handleDescriptionChange = (event) => {
    setDescription(event.target.value);
  };
  const handleLinkChange = (event) => {
    setLink(event.target.value);
  };

  async function onSubmit(event) {
    event.preventDefault();
    console.log('sdsd');
    const newEditedCard = {
      id: tabs.activeCard.id,
      header,
      content: description,
      url: link,
    };
    await editCard(newEditedCard);
    tabs.editActiveCard(newEditedCard);
    tabs.setActiveCard({});
    closeModal();

    event.preventDefault();
  }
  return (
    <div className={styles.modal}>
      <div className={styles.modalHeaderContainer}>
        <div className={styles.modalHeaderContent}>
          Редактировать
        </div>
        <div>
          <Button onClick={closeModal} color="secondary">
            X
          </Button>
        </div>
      </div>
      <form onSubmit={onSubmit}>
        <div className={styles.contentItemContainer}>
          <div className={styles.imgContainer}>
            <img src={activeCard.img} alt="img" />
          </div>
          <div className={styles.textContent}>
            <div className={styles.textFieldWrapper}>
              <TextField
                className={styles.MuiFormControlroot}
                id="standard-multiline-flexible"
                label="Header"
                multiline
                rowsMax={2}
                value={header}
                onChange={handleHeaderChange}
              />
            </div>
            <div className={styles.textFieldWrapper}>
              <TextField
                className={styles.MuiFormControlroot}
                id="standard-multiline-flexible"
                label="Description"
                multiline
                rowsMax={4}
                value={description}
                onChange={handleDescriptionChange}
              />
            </div>
            <div className={styles.textFieldWrapper}>
              <TextField
                className={styles.MuiFormControlroot}
                id="standard-multiline-flexible"
                label="Link"
                multiline
                rowsMax={1}
                value={link}
                onChange={handleLinkChange}
              />
            </div>
          </div>
        </div>
        <div className={styles.formFooter}>
          <Button type="submit" variant="contained" color="primary">
            Редактировать
          </Button>
        </div>
      </form>
    </div>
  );
}
