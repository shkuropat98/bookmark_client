import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import {
  Formik,
  Form,
} from 'formik';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import { useHistory, NavLink } from 'react-router-dom';
import styles from './RegistrationForm.module.css';
import { registration } from '../../http/userAPI';
import { authInitialValues } from '../../utils/validations/const';
import { authSchema } from '../../utils/validations/validations';

async function registrateUser(login, password) {
  const response = await registration(login, password);
  return response;
}

function RegistrationForm() {
  const [open, setOpen] = useState(false);
  const [alertMessage, setAlertMessage] = useState('');
  const history = useHistory();
  return (
    <div className={styles.outerWrapper}>
      <div className={styles.wrapper}>
        <div className={styles.header}>
          <Button onClick={() => history.push('/welcome')} style={{ marginRight: '20px' }} variant="contained" color="primary">На главную</Button>
        </div>
        <div className={styles.container}>
          <div className={styles.formHeader}>
            Регистрация
          </div>
          <Formik
            initialValues={authInitialValues}
            validationSchema={authSchema}
            onSubmit={async (values) => {
              try {
                const response = await registrateUser(values.login, values.password);
                if (response instanceof Error) {
                  setOpen(true);
                  setAlertMessage(response.message);
                } else {
                  history.push('/auth');
                }
              } catch (error) {
                console.log(error.message);
              }
            }}
          >
            {({
              values,
              handleChange,
              errors,
              touched,
              handleBlur,
            }) => (
              <Form className={styles.form}>
                <div className={`${styles.inputContainer} ${styles.mb10}`}>
                  <TextField
                    fullWidth
                    className={styles.MuiFormControlroot}
                    id="login"
                    name="login"
                    label="Логин"
                    value={values.login}
                    onChange={handleChange}
                    error={touched.login && Boolean(errors.login)}
                    helperText={touched.login && errors.login}
                    onBlur={handleBlur}
                  />
                </div>
                <div className={`${styles.inputContainer} ${styles.mb10}`}>
                  <TextField
                    fullWidth
                    className={styles.MuiFormControlroot}
                    id="password"
                    label="Пароль"
                    name="password"
                    type="password"
                    value={values.password}
                    onChange={handleChange}
                    error={touched.password && Boolean(errors.password)}
                    helperText={touched.password && errors.password}
                    onBlur={handleBlur}
                  />
                </div>
                <div className={styles.formFooter}>
                  <Button type="submit" variant="contained" color="primary">
                    Зарегистрироваться
                  </Button>
                </div>
                <div>
                  Есть аккаунт?
                  {' '}
                  <NavLink to="/auth" style={{ cursor: 'pointer', color: 'blue' }}>Войти!</NavLink>
                </div>
              </Form>
            )}
          </Formik>
          <Snackbar open={open} autoHideDuration={2000} onClose={() => setOpen(false)}>
            <Alert onClose={() => setOpen(false)} severity="error">
              {alertMessage}
            </Alert>
          </Snackbar>
        </div>
      </div>
    </div>
  );
}
export default RegistrationForm;
