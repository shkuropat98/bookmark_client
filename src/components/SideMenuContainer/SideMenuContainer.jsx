import React from 'react';
import styles from './SideMenuContainer.module.css';

export default function SideMenuContainer(props) {
  const { collectionsList } = props;
  return (
    <div className={styles.sideWrapper}>
      {collectionsList}
    </div>
  );
}
