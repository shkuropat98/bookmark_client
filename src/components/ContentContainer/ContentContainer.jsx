import React from 'react';
import styles from './contentContainer.module.css';

export default function ContentContainer(props) {
  const { children } = props;
  return (
    <div className={styles.contentContainer}>
      {children}
    </div>
  );
}
