/* eslint-disable max-len */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from 'react';
import TabItem from '../TabItem/TabItem';
import styles from './CollectionsStyles.module.css';

export default function CollectionsList(props) {
  const {
    tabs,
    activeTabIndex,
    setActiveTabIndex,
  } = props;

  return (
    <div className={styles.collectionsListContainer}>
      <div className={styles.header}>
        Мои коллекции
      </div>
      {tabs.map((tab, index) => (
        index === activeTabIndex
          ? (
            <TabItem
              key={tab.label}
              id={index}
              setActiveTabIndex={setActiveTabIndex}
              label={tab.label}
              active
            />
          )
          : (
            <TabItem
              key={tab.label}
              id={index}
              setActiveTabIndex={setActiveTabIndex}
              label={tab.label}
            />
          )
      ))}

    </div>
  );
}
