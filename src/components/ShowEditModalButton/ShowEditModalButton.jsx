/* eslint-disable max-len */
/* eslint-disable import/no-cycle */
import React, { useContext } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import {
  Formik,
  Form,
} from 'formik';
import styles from './ShowEditModalButton.module.css';
import { createNewCard } from '../../http/cardAPI';
import { MyContext } from '../../index';
import { addNewCardSchema } from '../../utils/validations/validations';
import { addNewCardInitialValues } from '../../utils/validations/const';

export default function ShowEditModalButton(props) {
  const { tabs } = useContext(MyContext);
  return (
    <Formik
      initialValues={addNewCardInitialValues}
      validationSchema={addNewCardSchema}
      validateOnBlur
      validateOnChange
      onSubmit={async (values, actions) => {
        try {
          const currentCollectionId = tabs.activeCollectionTab.id;
          const newCard = await createNewCard(values.cardUrl, currentCollectionId);
          tabs.addCardInCurrentCollection(currentCollectionId, newCard);
          // actions.resetForm();
          console.log(newCard);
        } catch (error) {
          console.log(error);
        }
      }}
    >
      {
        ({
          values,
          handleChange,
          errors,
          touched,
          actions,
        }) => (
          <Form className={styles.container}>
            <div className={styles.input}>
              <TextField
                fullWidth
                placeholder="url"
                value={values.cardUrl}
                id="cardUrl"
                name="cardUrl"
                onChange={handleChange}
                autoComplete="off"
                error={touched.cardUrl && Boolean(errors.cardUrl)}
                helperText={touched.cardUrl && errors.cardUrl}
              />
            </div>
            <Button type="submit" variant="contained" color="primary">
              Добавить закладку
            </Button>
          </Form>
        )
      }
    </Formik>
  );
}
