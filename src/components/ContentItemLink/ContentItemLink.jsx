/* eslint-disable import/no-cycle */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable max-len */
/* eslint-disable jsx-a11y/anchor-has-content */
/* eslint-disable jsx-a11y/control-has-associated-label */
import React, { useContext } from 'react';
import { FiGlobe } from 'react-icons/fi';
import { AiFillEdit, AiFillDelete } from 'react-icons/ai';
import { observer } from 'mobx-react-lite';
import { MyContext } from '../../index';
import styles from './ContentItemLink.module.css';
import { deleteCurrentCard } from '../../http/cardAPI';

const ContentItem = observer(({
  card,
}) => {
  const { tabs } = useContext(MyContext);
  const deleteCard = async (cardId) => {
    try {
      const deletedRows = await deleteCurrentCard(cardId);
      tabs.deleteCard(cardId);
      tabs.setActiveCard({});
    } catch (error) {
      console.log(error);
    }
  };
  const openEditModal = (isOpen) => {
    tabs.setOpenEditModal(isOpen);
  };

  return (
    <div
      className={card.id === tabs.activeCard.id ? (`${styles.activeCard} ${styles.ItemWrapper}`) : `${styles.ItemWrapper}`}
      onClick={() => {
        tabs.setActiveCard({
          id: card.id,
          content: card.content,
          header: card.header,
          img: card.img,
          url: card.url,
        });
      }}
    >
      <div className={styles.linkItemContainer}>
        <div className={styles.ItemImage}>
          <img src={card.img} alt="temp" />
        </div>
        <div className={styles.ItemTextContent}>
          <div className={styles.header}>
            {card.header}
          </div>
          <div className={styles.description}>
            {card.content}
          </div>
          <div className={styles.link}>
            {card.url}
          </div>
        </div>
      </div>
      {
        card.id === tabs.activeCard.id ? (
          <div className={styles.buttonsContainer}>
            <div style={{ marginRight: 10 }}>
              <a href={tabs.activeCard.url} target="_blank" rel="noreferrer">
                <FiGlobe />
              </a>
            </div>
            <div onClick={() => { openEditModal(true); }} style={{ marginRight: 10 }}><AiFillEdit /></div>
            <div onClick={() => deleteCard(tabs.activeCard.id)}><AiFillDelete /></div>
          </div>
        ) : null
      }
      {/* <a target="_blank" className={styles.realLink} href={contentObject.link} rel="noreferrer" /> */}
    </div>
  );
});
export default ContentItem;
