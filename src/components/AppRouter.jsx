/* eslint-disable import/no-cycle */
/* eslint-disable max-len */
import React, { useContext } from 'react';
import {
  Redirect, Switch, Route,
} from 'react-router-dom';
import { observer } from 'mobx-react-lite';
import { MyContext } from '../index';
import { authRoutes, publicRoutes } from '../routes';
import { WELCOME_ROUTE } from '../utils/const';

const AppRouter = observer(() => {
  const { user } = useContext(MyContext);

  return (
    <Switch>
      {user.isAuth && authRoutes.map(({ path, Component }) => <Route key={path} path={path} component={Component} exact />)}
      {publicRoutes.map(({ path, Component }) => <Route key={path} path={path} component={Component} exact />)}
      <Redirect to={WELCOME_ROUTE} />
    </Switch>
  );
});

export default AppRouter;
