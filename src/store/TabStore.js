/* eslint-disable no-param-reassign */
/* eslint-disable max-len */
/* eslint-disable no-underscore-dangle */
import { makeAutoObservable } from 'mobx';

export default class tabStore {
  constructor() {
    this._collectionsWithCards = [];
    this._activeCollectionTab = {
      id: null,
      title: null,
    };
    this._activeCard = {
      id: null,
      content: null,
      header: null,
      img: null,
      url: null,
    };
    this._openEditModal = false;
    makeAutoObservable(this);
  }

  setOpenEditModal(bool) {
    this._openEditModal = bool;
  }

  setActiveCard(card) {
    this._activeCard = card;
  }

  setCollectionsWithCards(collectionsWithCards) {
    this._collectionsWithCards = collectionsWithCards;
  }

  setActiveCollectionTab(activeCollectionTab) {
    this._activeCollectionTab = activeCollectionTab;
  }

  createNewCollection(newCollection) {
    this._collectionsWithCards = [...this._collectionsWithCards, newCollection];
  }

  deleteCollection(collectionId) {
    const filteredArray = this._collectionsWithCards.filter((item) => item.id !== collectionId);
    this._collectionsWithCards = filteredArray;
  }

  deleteCard(deletedCardId) {
    const collectionsWithoutCurrentCard = this._collectionsWithCards.map((item) => {
      if (item.id === this._activeCollectionTab.id) {
        const filteredCards = item.cards.filter((card) => card.id !== deletedCardId);
        item.cards = filteredCards;
      }
      return item;
    });
    this._collectionsWithCards = collectionsWithoutCurrentCard;
  }

  editActiveCard(newCard) {
    const collectionsWithEditedCard = this._collectionsWithCards.map((collection) => {
      if (collection.id === this._activeCollectionTab.id) {
        const collectionWithEditedCard = collection.cards.map((card) => {
          if (card.id === this._activeCard.id) {
            const editedCard = { ...this._activeCard, ...newCard };
            card = editedCard;
          }
          return card;
        });
        collection.cards = collectionWithEditedCard;
      }
      return collection;
    });
    this._collectionsWithCards = collectionsWithEditedCard;
  }

  editActiveCollection(newTitle) {
    const collectionsWithEditedCollection = this._collectionsWithCards.map((collection) => {
      if (collection.id === this._activeCollectionTab.id) {
        collection.title = newTitle;
      }
      return collection;
    });
    this._collectionsWithCards = collectionsWithEditedCollection;
  }

  addCardInCurrentCollection(collectionId, newCard) {
    const collectionsWithNewCard = this._collectionsWithCards.map((item) => {
      if (item.id === collectionId) {
        if (!item.cards) {
          item.cards = [];
        }
        item.cards.push(newCard);
      }
      return item;
    });
    this._collectionsWithCards = collectionsWithNewCard;
  }

  get activeCollectionTab() {
    return this._activeCollectionTab;
  }

  get collectionsWithCards() {
    return this._collectionsWithCards;
  }

  get activeCard() {
    return this._activeCard;
  }

  get openEditModal() {
    return this._openEditModal;
  }
}
