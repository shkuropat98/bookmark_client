/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable max-len */
/* eslint-disable no-shadow */
/* eslint-disable import/no-cycle */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/tabindex-no-positive */
import React, { useContext, useState } from 'react';
import './Tabs.css';
import { observer } from 'mobx-react-lite';
import { AiFillDelete } from 'react-icons/ai';
import TextField from '@material-ui/core/TextField';
import { useDoubleTap } from 'use-double-tap';
import Button from '@material-ui/core/Button';
import {
  Formik,
  Form,
} from 'formik';
import ContentItemLink from '../components/ContentItemLink/ContentItemLink';
import { MyContext } from '../index';
import { editCollection, deleteCollection } from '../http/collectionAPI';
import { editCollectionSchema } from '../utils/validations/validations';

export const TabItem = ({
  tabTitle, tabIndex, toggleState, setToggleState,
}) => {
  const { tabs } = useContext(MyContext);
  const [editCollectionModal, setEditCollectionModal] = useState(false);
  const editCollectionInitialValues = {
    collectionTitle: tabs.activeCollectionTab.title,
  };

  const deleteColl = async (collectionId, tabs) => {
    await deleteCollection(collectionId);
    tabs.deleteCollection(collectionId);
  };

  const editCollectionMode = () => {
    if (editCollectionModal) {
      setEditCollectionModal(false);
    } else {
      setEditCollectionModal(true);
    }
  };
  const enableEditModeByDoubleTab = useDoubleTap((event) => {
    editCollectionMode();
  });

  return (
    <div
      className={toggleState === tabIndex ? 'tabs active-tabs' : 'tabs'}
      onClick={() => {
        setToggleState(tabIndex);
        tabs.setActiveCollectionTab({
          id: tabIndex,
          title: tabTitle,
        });
        if (toggleState !== tabIndex) { setEditCollectionModal(false); }
      }}
    >
      <div
        {...enableEditModeByDoubleTab}
        onDoubleClick={() => { editCollectionMode(); }}
        className="collectionMode"
      >
        {(toggleState === tabIndex && editCollectionModal) ? (
          <Formik
            initialValues={editCollectionInitialValues}
            validationSchema={editCollectionSchema}
            validateOnBlur={false}
            validateOnChange={false}
            onSubmit={async (values) => {
              try {
                const newCollection = { id: tabs.activeCollectionTab.id, title: values.collectionTitle };
                await editCollection(newCollection);
                tabs.setActiveCollectionTab(newCollection);
                tabs.editActiveCollection(values.collectionTitle);
                setEditCollectionModal(false);
                editCollectionMode();
              } catch (error) {
                console.log(error);
              }
            }}
          >
            {
                ({
                  values,
                  handleChange,
                  errors,
                  touched,
                }) => (
                  <Form className="editCollectionFormContainer">
                    <TextField
                      value={values.collectionTitle}
                      id="collectionTitle"
                      name="collectionTitle"
                      onChange={handleChange}
                      autoComplete="off"
                      error={touched.collectionTitle && Boolean(errors.collectionTitle)}
                      helperText={touched.collectionTitle && errors.collectionTitle}
                      style={{ marginRight: '10px', marginBottom: '10px' }}
                    />
                    <Button className="editCollectionAdmitButton" size="small" type="submit" variant="outlined" color="primary">
                      ОК
                    </Button>
                  </Form>
                )
              }
          </Formik>
        ) : tabTitle}
        {
        toggleState === tabIndex
          ? (
            <div className="deleteIconButton">
              <AiFillDelete onClick={() => deleteColl(tabs.activeCollectionTab.id, tabs)} />
            </div>
          ) : null
        }
      </div>
    </div>
  );
};

export const TabContent = ({
  tabIndex, toggleState, cards,
}) => (
  <div className={toggleState === tabIndex ? 'content  active-content' : 'content'}>
    {
      cards ? (
        cards.map((card) => (
          <ContentItemLink
            key={card.id + card.name}
            card={card}
          />
        ))) : null
    }
  </div>
);
const Tabs = observer(({ tabs }) => {
  const [toggleState, setToggleState] = useState(tabs.collectionsWithCards[0].id);
  const [activeCard, setActiveCard] = useState(0);
  return (
    <div className="container">
      <div className="bloc-tabs">
        {
            tabs.collectionsWithCards.map((collection) => (
              <TabItem
                key={collection.id + collection.title}
                tabTitle={collection.title}
                tabIndex={collection.id}
                cards={collection.cards}
                toggleState={toggleState}
                setToggleState={setToggleState}
                tabs={tabs}
              />
            ))
        }
      </div>
      <div className="content-tabs">
        {
            tabs.collectionsWithCards.map((collection) => (
              <TabContent
                key={`${collection.id + collection.title}kek`}
                tabIndex={collection.id}
                cards={collection.cards}
                toggleState={toggleState}
                activeCard={activeCard}
                setActiveCard={setActiveCard}
              />
            ))
        }
      </div>
    </div>
  );
});

export default Tabs;
