/* eslint-disable import/no-cycle */
import { observer } from 'mobx-react-lite';
import React from 'react';
import AuthForm from '../components/AuthForm/AuthForm';

const Auth = observer(() => (<AuthForm />));
export default Auth;
