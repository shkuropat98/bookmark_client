/* eslint-disable jsx-a11y/tabindex-no-positive */
/* eslint-disable import/no-cycle */
import Button from '@material-ui/core/Button';
import React, { useContext, useState, useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import { useHistory } from 'react-router-dom';
import { MyContext } from '../index';
import { WELCOME_ROUTE } from '../utils/const';
import { fetchCollectionsWithCards } from '../http/collectionAPI';
import Tabs, { TabItem, TabContent } from './Tabs';
import styles from './Main.module.css';
import UserInfo from '../components/UserInfo/UserInfo';
import AddCollectionForm from '../components/AddCollectionForm/AddCollectionForm';
import ShowEditModalButton from '../components/ShowEditModalButton/ShowEditModalButton';
import Modal from '../components/Modal/Modal';

const Main = observer(() => {
  const { tabs, user } = useContext(MyContext);
  const history = useHistory();
  const [loading, setLoading] = useState(true);
  const [toggleState, setToggleState] = useState(null);
  const [showModalEdit, setShowModalEdit] = useState(false);
  const [url, setUrl] = useState('url');

  const logOut = () => {
    user.setUser({});
    user.setIsAuth(false);
    tabs.setCollectionsWithCards({});
    history.push(WELCOME_ROUTE);
    localStorage.removeItem('token');
  };

  useEffect(() => {
    fetchCollectionsWithCards().then((data) => {
      const { collectionsWithCards } = data;
      tabs.setCollectionsWithCards(collectionsWithCards);
      setToggleState(tabs.collectionsWithCards[0].id);
      tabs.setActiveCollectionTab({
        id: tabs.collectionsWithCards[0].id,
        title: tabs.collectionsWithCards[0].title,
      });
    }).finally(() => setLoading(false));
  }, []);

  if (loading) {
    return (<div>Loading...</div>);
  }

  return (
    <div className={styles.container}>
      {tabs.openEditModal
        && (
        <div className={styles.modal}>
          <Modal activeCard={tabs.activeCard} />
        </div>
        )}
      <div className={styles.side}>
        <Button onClick={logOut}>Выйти</Button>
        <UserInfo username={user.user.email} />
        <AddCollectionForm />
        <div className={styles.collectionsHeader}>
          Мои коллекции
        </div>
        {
            tabs.collectionsWithCards.map((collection) => (
              <TabItem
                tabTitle={collection.title}
                tabIndex={collection.id}
                cards={collection.cards}
                toggleState={toggleState}
                setToggleState={setToggleState}
              />
            ))
        }
      </div>
      <div className={styles.content}>
        <ShowEditModalButton
          url={url}
          setUrl={setUrl}
        />
        <div className={styles.contentHeader}>
          {tabs.activeCollectionTab.title}
        </div>
        <div>
          {
            tabs.collectionsWithCards.map((collection) => (
              <TabContent
                tabIndex={collection.id}
                cards={collection.cards}
                toggleState={toggleState}
              />
            ))
        }
        </div>
      </div>
    </div>
  );
});

export default Main;
