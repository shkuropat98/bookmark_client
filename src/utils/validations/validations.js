import * as yup from 'yup';

export const authSchema = yup.object().shape({
  login: yup
    .string('Введите логин')
    .required('Требуется логин'),
  password: yup
    .string('Введите пароль')
    .min(3, 'Минимум 3 символа')
    .required('Требуется пароль')
    .max(16, 'Максимум 16 символов'),
});
export const addCollectionSchema = yup.object().shape({
  collectionTitle: yup
    .string('Введите имя коллекции')
    .required('Требуется имя')
    .max(16, 'Максимум 25 символов'),
});
export const editCollectionSchema = yup.object().shape({
  collectionTitle: yup
    .string('Введите имя коллекции')
    .required('Требуется имя')
    .max(16, 'Максимум 25 символов'),
});
export const addNewCardSchema = yup.object().shape({
  cardUrl: yup
    .string('Введите url'),
  // .required('Требуется url')
  // .url('Неправильный url'),
});
